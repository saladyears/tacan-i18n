Greater Tacan - Scenario
========

<ul>
	<li><a href="info/scenario/GreaterTacan#Overview">Overview</a>
	<li><a href="info/scenario/GreaterTacan#BoardSetup">Board Setup</a>
	<li><a href="info/scenario/GreaterTacan#Rules">Rules</a>
	<li><a href="info/scenario/GreaterTacan#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Great Tacan scenario includes the Mariners of Tacan expansion with rules for transferring roll chips from the mainland to outer islands.

It uses [Standard](/info/rules/Standard), [Mariners](/info/rules/Mariners), and [Chipless Tiles](/info/rules/Chipless_Tiles) rules.

The [Jungles](/info/rules/Jungles) and [Volcanos](/info/rules/Volcanos) variants are allowed.

It is played to **18 points** for [4](/static/images/scenarios/greatertacan4.jpg "Greater Tacan 4") or [6](/static/images/scenarios/greatertacan6.jpg "Greater Tacan 6") players.

<a id="BoardSetup"></a>

Board Setup
----

The board consists of a Standard 3-4 or 5-6 player board as a central island surrounded by smaller islands.

The surrounding islands have fixed locations, random tile types, and no roll chips.

The robber starts on the desert tile.

The pirate starts off the board.

### Ports

Ports are placed around the central island as they are for a Standard 3-4 or 5-6 player board.

<a id="Rules"></a>

Rules
----

Players start with __8__ cities.

Initial settlements can only be placed on the central island.

When a road or ship is built along a chipless tile, a new chip is taken from a pool, or, when the pool is exhausted, moved from the central island.

<a id="Notes"></a>

Notes
----

Greater Tacan is the biggest scenario in Tacan.  With 8 cities and 18 points to win, settle in for a long game!