Standard - Scenario
========

<ul>
	<li><a href="info/scenario/Standard#Overview">Overview</a>
	<li><a href="info/scenario/Standard#BoardSetup">Board Setup</a>
	<li><a href="info/scenario/Standard#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Standard scenario is the most basic way to play Colonists of Tacan.  

It uses [Standard](/info/rules/Standard) rules.

The [Jungles](/info/rules/Jungles) and [Volcanos](/info/rules/Volcanos) variants are allowed.

It is played to **10 points** with boards for [3-4](/static/images/scenarios/standard34.jpg "Standard 3-4") or [5-6](/static/images/scenarios/standard56.jpg "Standard 5-6") players.

<a id="BoardSetup"></a>

Board Setup
----

The board consists of 19 land tiles surrounded by 18 ocean tiles (30 and 22 for 5-6 player games).  At the start of the game, the land tiles are shuffled and laid out in a spiral starting from the center of the board.  This ensures a random board for every new game played.

The roll chips are placed starting from one of the six outside corners of the land tiles in a spiral back to the center, skipping any desert tiles.  Because the location of the desert(s) is random each game, the chip layout is also different for each new game.

The robber starts on the desert tile.

### Ports

Ports are shuffled and placed on every other ocean tile, starting from a randomly determined ocean tile, always facing the longest line of land tiles across the board.

<a id="Notes"></a>

Notes
----

If you are new to Tacan, Standard is the way to start!