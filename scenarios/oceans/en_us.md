Oceans - Scenario
========

<ul>
	<li><a href="info/scenario/Oceans#Overview">Overview</a>
	<li><a href="info/scenario/Oceans#BoardSetup">Board Setup</a>
	<li><a href="info/scenario/Oceans#Rules">Rules</a>
	<li><a href="info/scenario/Oceans#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Oceans scenario includes the Mariners of Tacan expansion and rewards seaward exploration.

It uses [Standard](/info/rules/Standard), [Mariners](/info/rules/Mariners), [Gold](/info/rules/Gold), and [Hidden Tiles](/info/rules/Hidden_Tiles) rules.

The [Jungles](/info/rules/Jungles) and [Volcanos](/info/rules/Volcanos) variants are allowed for the 4 player board.

It is played to **12 points** for [3](/static/images/scenarios/oceans3.jpg "Oceans 3"), [4](/static/images/scenarios/oceans4.jpg "Oceans 4"), [5](/static/images/scenarios/oceans5.jpg "Oceans 5"), or [6](/static/images/scenarios/oceans6.jpg "Oceans 6") players.

<a id="BoardSetup"></a>

Board Setup
----

The board consists of a main island with fixed locations, tile types, and roll chips surrounded by hidden and gold tiles.

The robber starts off the board.

The pirate starts off the board.

### Ports

Port locations are fixed but randomly assigned.

<a id="Rules"></a>

Rules
----

Building a ship or road that points at a hidden tile reveals it.  If the hidden tile is a resource, the player receives one of that resource.

Gold tiles allow players to trade gold received for any resource.

<a id="Notes"></a>

Notes
----

It is difficult to win Oceans without building at least some ships to explore for new land.