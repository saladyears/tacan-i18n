The Great Crossing - Scenario
========

<ul>
	<li><a href="info/scenario/GreatCrossing#Overview">Overview</a>
	<li><a href="info/scenario/GreatCrossing#BoardSetup">Board Setup</a>
	<li><a href="info/scenario/GreatCrossing#Rules">Rules</a>
	<li><a href="info/scenario/GreatCrossing#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Great Crossing scenario includes the Mariners of Tacan expansion with rules for establishing trade routes.

It uses [Standard](/info/rules/Standard), [Mariners](/info/rules/Mariners), and [Trade Routes](/info/rules/Trade_Routes) rules.

It is played to __13 points__ for [4](/static/images/scenarios/greatcrossing4.jpg "Great Crossing 4") or [6](/static/images/scenarios/greatcrossing6.jpg "Great Crossing 6") players.

<a id="BoardSetup"></a>

Board Setup
----

The board consists of two large "home" islands with fixed locations, tile types, and roll chips.

The robber starts off the board.

The pirate starts off the board.

### Ports

Port locations are fixed but randomly assigned.

<a id="Rules"></a>

Rules
----

Players must choose the same home island for both of their initial settlements.

The number of players on each home island must be identical (either 2/2 or 3/3).

Players receive bonus points for creating trade routes with ships and roads that link their settlements and cities on their home island to settlements and cities on the other island.

<a id="Notes"></a>

Notes
----

Trade Routes can be used in sneaky ways to steal a victory.  Make sure you understand how they work before playing The Great Crossing!