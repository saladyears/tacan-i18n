A New World - Scenario
========

<ul>
	<li><a href="info/scenario/NewWorld#Overview">Overview</a>
	<li><a href="info/scenario/NewWorld#BoardSetup">Board Setup</a>
	<li><a href="info/scenario/NewWorld#Rules">Rules</a>
	<li><a href="info/scenario/NewWorld#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
---

The New World scenario includes the Mariners of Tacan expansion and generates a random starting map each game.

It uses [Standard](/info/rules/Standard), [Mariners](/info/rules/Mariners), [Port Placement](/info/rules/Port_Placement), and [Single Bonus Point](/info/rules/Single_Bonus_Point) rules.

The [Skip Port Placement](/info/rules/Skip_Port_Placement) variant is allowed.

It is played to **12 points** for [3](/static/images/scenarios/newworld3.jpg "A New World 3"), [4](/static/images/scenarios/newworld4.jpg "A New World 4"), [5](/static/images/scenarios/newworld5.jpg "A New World 5"), or [6](/static/images/scenarios/newworld6.jpg "A New World 6") players.

<a id="BoardSetup"></a>

Board Setup
---

The board is randomly generated.  Roll chips are randomly placed on the generated board.  Chips with 6 or 8 rolls are placed so their tiles do not touch.

The robber starts off the board.

The pirate starts off the board.

### Ports

Port locations are determined by players at the start of the game.

A port may not share a corner with any other port.

<a id="Rules"></a>

Rules
---

Players receive one bonus point for their first new island colonized. 

<a id="Notes"></a>

Notes
---

Since A New World is randomly generated, there is a wide range in quality of initial starting positions.  Often players will need to place on two tiles and a port as their second placement.