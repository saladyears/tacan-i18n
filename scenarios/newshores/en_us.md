New Shores - Scenario
========

<ul>
	<li><a href="info/scenario/NewShores#Overview">Overview</a>
	<li><a href="info/scenario/NewShores#BoardSetup">Board Setup</a>
	<li><a href="info/scenario/NewShores#Rules">Rules</a>
	<li><a href="info/scenario/NewShores#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The New Shores scenario introduces the core concepts in the Mariners of Tacan expansion and rewards colonizing new islands.

It uses [Standard](/info/rules/Standard), [Mariners](/info/rules/Mariners), [Gold](/info/rules/Gold), and [Single Bonus Point](/info/rules/Single_Bonus_Point) rules.

The [Jungles](/info/rules/Jungles) and [Volcanos](/info/rules/Volcanos) variants are allowed.

It is played to **13 points** for [3-4](/static/images/scenarios/newshores34.jpg "New Shores") players.

<a id="BoardSetup"></a>

Board Setup
----

The board consists of a Standard 3-4 player board as the main island with smaller islands surrounding it.

The surrounding islands have fixed locations, tile types and roll chips.

The robber starts on the desert tile.

The pirate starts off the board.

### Ports

Ports are placed around the main island as they are for a Standard 3-4 player board.

<a id="Rules"></a>

Rules
----

Initial settlements can only be placed on the main island.

Players receive one bonus point for their first new island colonized. 

Gold tiles allow players to trade gold received for any resource.

<a id="Notes"></a>

Notes
----

New Shores is a great way to play the Mariners of Tacan expansion for the first time.