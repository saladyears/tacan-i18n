Into The Desert - Scenario
========

<ul>
	<li><a href="info/scenario/IntoTheDesert#Overview">Overview</a>
	<li><a href="info/scenario/IntoTheDesert#BoardSetup">Board Setup</a>
	<li><a href="info/scenario/IntoTheDesert#Rules">Rules</a>
	<li><a href="info/scenario/IntoTheDesert#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Into The Desert scenario includes the Mariners of Tacan expansion and rewards sea and land colonization.

It uses [Standard](/info/rules/Standard), [Mariners](/info/rules/Mariners), [Gold](/info/rules/Gold), and [Single Bonus Point](/info/rules/Single_Bonus_Point) rules.

The [Jungles](/info/rules/Jungles) and [Volcanos](/info/rules/Volcanos) variants are allowed and are common enough to be called _Into The Jungles_ and _Into the Volcanos_, respectively.

It is played to **12 points** for [3](/static/images/scenarios/intothedesert3.jpg "Into The Desert 3"), [4](/static/images/scenarios/intothedesert4.jpg "Into The Desert 4"), [5](/static/images/scenarios/intothedesert5.jpg "Into The Desert 5"), or [6](/static/images/scenarios/intothedesert6.jpg "Into The Desert 6") players.

<a id="BoardSetup"></a>

Board Setup
----

The board consists of a central landmass with fixed locations, tile types, and roll chips.  It is bordered by Deserts and smaller landmasses and islands with fixed locations, tile types, and roll chips.

The robber starts on one of the Deserts.

The pirate starts off the board.

### Ports

Port locations are fixed but randomly assigned.

<a id="Rules"></a>

Rules
----

Initial settlements can only be placed on the main landmass.

Players receive one bonus point for their first new island or landmass colonized.

Gold tiles allow players to trade gold received for any resource.

<a id="Notes"></a>

Notes
----

Play Into The Desert with [Jungles](/info/rules/Jungles) or [Volcanos](/info/rules/Volcanos) for a very unique Tacan experience!