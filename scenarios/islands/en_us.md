Islands - Scenario
========

<ul>
	<li><a href="info/scenario/Islands#Overview">Overview</a>
	<li><a href="info/scenario/Islands#BoardSetup">Board Setup</a>
	<li><a href="info/scenario/Islands#Rules">Rules</a>
	<li><a href="info/scenario/Islands#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Islands scenario includes the Mariners of Tacan expansion and heavily rewards colonization of new islands.

It uses [Standard](/info/rules/Standard), [Mariners](/info/rules/Mariners), [Coastal Ships](/info/rules/Coastal_Ships), and [Repeating Bonus Points](/info/rules/Repeating_Bonus_Points) rules.

It is played to **12 points** for [3](/static/images/scenarios/islands3.jpg "Islands 3"), [4](/static/images/scenarios/islands4.jpg "Islands 4"), [5](/static/images/scenarios/islands5.jpg "Islands 5"), or [6](/static/images/scenarios/islands6.jpg "Islands 6") players.

<a id="BoardSetup"></a>

Board Setup
----

The board consists of 4, 5, or 6 islands with fixed locations, tile types, and roll chips.

The robber starts off the board.

The pirate starts off the board.

### Ports

Port locations are fixed but randomly assigned.

<a id="Rules"></a>

Rules
----

When placing an initial road or ship on a coast (where a land and ocean tile meet), unless the coastline is along the edge of the board, a ship must be placed.

Players receive one bonus point for their first new island colonized.  They receive two bonus points for each additional island.

<a id="Notes"></a>

Notes
----

It is almost impossible to win Islands without colonizing at least one new island.