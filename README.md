# Colonists of Tacan Internationalization (i18n)

The Colonists of Tacan web game (at [http://tacan.alturnate.com](http://tacan.alturnate.com)) has been written to support full translation of the default American English game text into other languages.  It simply needs translators willing to do so.

If you are interested in providing a Tacan translation, you are at the right place.  There are two routes to getting a translation into the game: the simple way and the advanced way.  Steps for both are provided below.

The simple way is to translate the files and then just "send them in" via a .zip file attached to a JIRA ticket.  It is a "fire and forget" approach where you do the translation once and are done.  Your translated files will be added to the git repository by a Tacan admin.

The advanced way requires setting up an account on BitBucket and using git to access the project, but allows you to keep a history of changes as well as continue to make modifications to the translation files later on.  You submit pull requests via BitBucket to notify the Tacan admins that you have a translation ready to proceed.

Translators will receive credit on the game's website for any translation.

## How to translate

Regardless of which of the two approaches you use, the actual translation is straightforward.

This i18n repository is composed of .txt files that contain lines with translatable strings.  The .txt files are organized by what part of the game they provide text for (such as the lobby, system window, game, etc.).  The text files are named with the 2-character language code for the translation being provided, an underscore, and then the 2-character region code, all lower-case.  The default is en_us.txt for United States English.

Language and region codes are available here: [http://www.i18nguy.com/unicode/language-identifiers.html](http://www.i18nguy.com/unicode/language-identifiers.html)

Once you have identified your region and language, simply copy the existing en_us.txt file in each directory and rename the new file according to your language and region.

For example, if you are creating a translation for German in Germany, you would create files called de_de.txt.  For German in Austria, you would create files called de_at.txt.  Belgian German would be de_be.txt, and so on.

Once you have copied the existing en_us.txt files and renamed them, you are now ready to begin translation.

Each translatable line consists of a key, a space, an equals sign, a space, and the text to translate.  For example, the bonus point game rules have only three translatable pieces of text:

    ##
    ## Player UI.
    ##

    # Icons.
    BonusPoint = Bonus points

    ##
    ## History.
    ##

    ReceivedOneBonusPoint = {0} received 1 bonus point for a new colony!
    ReceivedXBonusPoints = {0} received {1} bonus points for a new colony!

To translate, simply replace the text to the right of the equals sign with the translation.  It is critical that there be one space before and one space after the equals sign or the game will not be able to process the line.  You must also not change any of the key text left of the equals sign (leave it as English).  If you do so, the game will not be able to look up your translation.

Comment lines begin with the # symbol.  They are ignored.

If you see a {0}, {1}, etc., in a line, those placeholders will be filled in with a value by the game, often a player's name or number count of something.  You must put these placeholders in your translation in the appropriate location.

## The Simple Way

To use the simple way of translating, follow these steps:

1.  Download the current repository files from the download link on the right side of the page.
2.  Unzip the repository into a directory of your choice.
3.  Copy the en_us.txt file in each directory and rename it according to your translation's region and language code.
4.  Do the translation work.
5.  Zip up the entire directory structure containing your translated files and call it en_us.zip, replacing en_us with your translation's region and language code.
6.  Open a JIRA ticket at [http://tacan.alturnate.com/jira/](http://tacan.alturnate.com/jira/).  
    * Login with the user name of 'i18n' and password 'i18n' (without quotes).  
    * Click the Create Issue link in the upper-right.
    * Change the Issue Type to Improvement.
    * Select I18n in the Component dropwdown.
    * Enter the text "Translation to <your language" in the Summary.
    * Enter your name in the Description box if you want credit for the work.
    * Attach your .zip file in the Attachment line by clicking on the Browse box.
    * Click Create.  You are done!

Your translation will be added to the git repository and then enabled on the website by a Tacan admin.