﻿##
## Ruleset
##

HIDDEN_TILES_NAME = Apslēptie laukumi
HIDDEN_TILES_DESC = Izpēti karti, lai atklātu jaunas teritorijas. Kurš pirmais atklās jaunu resursu lauciņu, no tā iegūs vienu attiecīgo resursu par brīvu!

##
## History.
##

Discovered = {0} atklāja {1}.
ReceivedOne = {0} par atklājumu saņēma 1 {1}!
