##
## Ruleset
##

DEVELOPMENT_CARDS_NAME = Development Cards
DEVELOPMENT_CARDS_DESC = Buy development cards that may dramatically affect your chances of winning.

##
## Player UI.
##

# Icons.
ArmySize = Army size

# Buys.
DevelopmentCard = Card

# Cards.
Monopoly = Monopoly
RoadBuilding = Road Building
Soldier = Soldier
VictoryPoint = Victory Point
YearOfPlenty = Year Of Plenty

MonopolyText = Choose a resource.  All players must give you all their resource cards of that type.
RoadBuildingText = Build 2 roads this turn for free.
SoldierText = Move the robber to a new tile and steal a resource card from an adjacent player.
VictoryPointText = Receive 1 Victory Point!
YearOfPlentyText = Take 2 resources from the bank for free.

# Actions.
RollDice = Roll dice

##
## History.
##

BoughtADevCard = {0} bought a Development Card.
YouBoughtADevCard = You bought a {0} card.
PlayerBoughtADevCard = {0} bought a {1} card.

PlayedACard = {0} played a {1} card.

StoleNothing = {0} stole nothing.

GainedLargestArmy = {0} gained Largest Army.
LostLargestArmy = {0} lost Largest Army.

##
## Alerts.
##

WaitingRollDiceOrPlayCard = Waiting for {0} to roll the dice or play a card.
YouRollDiceOrPlayCard = {0}, roll the dice or play a card.

WaitingSelectTwoResourcesToGain = Waiting for {0} to select 2 resources to gain.
YouSelectTwoResourcesToGain = {0}, select 2 resources to gain.

WaitingSelectAResourceToSteal = Waiting for {0} to select a resource to steal.
YouSelectAResourceToSteal = {0}, select a resource to steal.
