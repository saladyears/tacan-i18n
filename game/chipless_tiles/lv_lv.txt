﻿##
## Ruleset
##

CHIPLESS_TILES_NAME = Nenumurēti lauciņi
CHIPLESS_TILES_DESC = Paplašini savu impēriju, pārceļot savas galvenā kontinenta lauku numurus uz jaunatklātajām teritorijām.

##
## History.
##

RevealedANewChip = {0} atklāja skaitli {1}!
TookAChipFromTheMainland = {0} pārvietoja skaitli {1}!

##
## Alerts.
##

WaitingSelectAMainlandChip = Gaida uz {0} skaitļa izvēli.
YouSelectAMainlandChip = {0}, izvēlies galvenā kontinenta skaitli.
