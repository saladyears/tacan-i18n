Gold - Rules
====

<ul>
	<li><a href="info/rules/Gold#Overview">Overview</a>
	<li><a href="info/rules/Gold#GameBoard">Game Board</a>
	<li><a href="info/rules/Gold#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Gold rules add a Gold tile to the board.

They are used in the [New Shores](/info/scenario/NewShores), [Oceans](/info/scenario/Oceans), and [Into The Desert](/info/scenario/IntoTheDesert) scenarios.

The [Volcanos](/info/rules/Volcanos) variant uses Gold.

<a id="GameBoard"></a>

Game Board
----

### Resources
<img src="/static/images/rules/gold.png" title="Gold"/>

When the number on a Gold tile is rolled, each player with a city or settlement on the tile receives gold, __1__ for each settlement, __2__ for each city.  Gold does not go into the player's hand.  Instead, the player immediately trades it to the bank for whatever resource(s) they want, in a 1 to 1 ratio of gold per resource.

If multiple players receive gold on the same turn, they trade in their gold one at a time, going in turn order and starting with the current player.

If the bank is out of a resource, gold cannot be traded for it.

<a id="Notes"></a>

Notes
----

Gold is extremely powerful so it is usually difficult to obtain or has an equivalent risk ([Volcanos](/info/rules/Volcanos)).