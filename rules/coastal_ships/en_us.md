Coastal Ships - Rules
====

<ul>
	<li><a href="info/rules/Coastal_Ships#Overview">Overview</a>
	<li><a href="info/rules/Coastal_Ships#InitialPlacements">Initial Placements</a>
	<li><a href="info/rules/Coastal_Ships#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Coastal Ship rules require players to build ships where possible during initial placements.

They are used in the [Islands](/info/scenario/NewShores) scenario.

<a id="InitialPlacements"></a>

Initial Placements
----

During initial placements, if a player places a piece along a coast, where a land and ocean tile meet, the player must place a ship:

<img src="/static/images/rules/coastalships1.jpg" title="The player must place a ship"/>

The only exception is if the coast touches the edge of the board.  In that case, the player may place either a road or a ship:

<img src="/static/images/rules/coastalships2.jpg" title="The player may place a road or ship"/>

<a id="Notes"></a>

Notes
----

Coastal Ships set the game up for rapid maritime expansion.