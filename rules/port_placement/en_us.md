Port Placement - Rules
====

<ul>
	<li><a href="info/rules/Port_Placement#Overview">Overview</a>
	<li><a href="info/rules/Port_Placement#GameBoard">Game Board</a>
	<li><a href="info/rules/Port_Placement#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Port Placement rules let players manually place ports on the board at the start of the game.

They are used in the [A New World](/info/scenario/NewWorld) scenario.

<a id="GameBoard"></a>

Game Board
----

At the start of the game, before initial placements, players take turn placing ports on ocean tiles on the board.

When placing ports, the actual type of port is not revealed until all ports have been placed.  When all ports have been placed, the port types are revealed simultaneously.

Only one port may be placed on any given ocean tile, and no ports may share corners with each other.

After ports have been revealed, the order of the players is shuffled before initial settlements are placed.  This prevents any player from gaining an advantage by being able to place both the last port and the first settlement.

<a id="Notes"></a>

Notes
----

On randomly generated maps, port placements can be crucial to a winning strategy, since it is unlikely there will be an abundance of great starting locations.

For those players who wish to skip manual port placement and have the computer randomly generate ports, there is a [Skip Port Placement](/info/rules/Skip_Port_Placement) variant to do so.