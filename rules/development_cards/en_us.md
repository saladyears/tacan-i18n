Development Cards - Rules
====

<ul>
	<li><a href="info/rules/Development_Cards#Overview">Overview</a>
	<li><a href="info/rules/Development_Cards#DevelopmentCards">Development Cards</a>
	<li><a href="info/rules/Development_Cards#LargestArmy">Largest Army</a>
	<li><a href="info/rules/Development_Cards#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Development Cards give players a variety of strategic options for gaining resources and earning points.

Unless disabled by another ruleset, they are automatically included with the [Standard](/info/rules/Standard) rules.

<a id="DevelopmentCards"></a>

Development Cards
----

Players may purchase any number of Development Cards in a single turn, as long as there are cards left to purchase.  

<img src="/static/images/rules/developmentcard.png" title="1 Sheep + 1 Wheat + 1 Ore = 1 Development Card"/>

A Development Card costs __1 Sheep__, __1 Wheat__, and __1 Ore__.

Development Cards may be purchased by a player on their turn using the __Buy__ menu:

<img src="/static/images/rules/buycard.png" title="Buying a Development Card"/>

Newly purchased Development Cards may not be played until the player's next turn, and only one Development Card may be played per turn.  Victory Points are an exception to both rules.

Development Cards are played using the __Cards__ menu by clicking on the card to play:

<img src="/static/images/rules/playcard.png" title="Playing a Soldier card"/>

A Development Card may be played before or after the dice are rolled, though typically only Soldier cards are played before the dice roll.

If a player has any Development Card in hand, the game will refrain from automatically rolling the dice.  This prevents opponents from guessing what kind of Development Card(s) the player has.  The player must choose to play a card or click __Roll Dice__ from the __Actions__ menu to advance.

Development Cards cannot be traded with other players.

There are five kinds of Development Cards available for purchase, listed below.  The cards are randomly shuffled before the start of each game.

<div class="card">
	<img class="left" src="/static/images/cards/monopoly.png" title="Monopoly Card"/>

	<h3>Monopoly</h3>

	<p>The Monopoly card lets the player choose <b>1</b> resource.  Every other player must then give that player all of their resource cards of that type.  

	<p>There are <b>2</b> Monopoly cards in a 3-4 player game and <b>3</b> in a 5-6 player game.
</div>

<div class="card">
	<img class="left" src="/static/images/cards/roadbuilding.png" title="Road Building Card"/>

	<h3>Road Building</h3>
	
	<p>The Road Building card allows the player to place <b>2</b> roads for free.  The player still uses the <b>Buy</b> menu to "purchase" the roads, but they cost no resources.

	<p>There are <b>2</b> Road Building cards in a 3-4 player game and <b>3</b> in a 5-6 player game.
</div>

<div class="card">
	<img class="left" src="/static/images/cards/soldier.png" title="Soldier Card"/>

	<h3>Soldier</h3>

	<p>The Soldier card lets the player move the robber and steal a card from an opponent as if a 7 had been rolled.

	<p>The number of Soldiers a player has played is tracked in their player area:
	<img src="/static/images/rules/soldierplayed.png" title="1 Soldier played"/>

	<p>There are <b>14</b> Soldier cards in a 3-4 player game and <b>20</b> in a 5-6 player game.
</div>

<div class="card">
	<img class="left" src="/static/images/cards/yearofplenty.png" title="Year Of Plenty Card"/>

	<h3>Year Of Plenty</h3>

	<p>The Year Of Plenty card allows the player to take any <b>2</b> available resources from the bank.

	<p>There are <b>2</b> Year Of Plenty cards in a 3-4 player game and <b>3</b> in a 5-6 player game.
</div>

<div class="card">
	<img class="left" src="/static/images/cards/victorypoint.png" title="Victory Point Card"/>

	<h3>Victory Point</h3>

	<p>A Victory Point counts as <b>1 point</b> towards winning the game.

	<p>Victory Points are not revealed in the player's point total during the game, unless played by the player.  However, the game does keep track of unrevealed Victory Point cards in the player's hand.  If a player with a Victory Point in hand gets enough points to win the game if the card was played, the game will reveal the card and award the win to the player.

	<p>There is usually no reason to actually play a Victory Point card, though the player is, of course, free to do so.  They are essentially "secret" points that are revealed at the end of the game.

	<p>The number of Victory Points a player has played is tracked in their player area:
	<img src="/static/images/rules/victorypointsplayed.png" title="2 Victory Points played"/>

	<p>There are <b>5</b> Victory Point cards in a game.
</div>

<a id="LargestArmy"></a>

Largest Army
----

The first player to play __3__ Soldier cards gains the Largest Army.

Holding the Largest Army is worth __2 points__ towards winning the game.

If another player plays _more_ Soldier cards (ties don't count), they take Largest Army, gaining two points while the previous Largest Army holder loses two points.

If any player holds Largest Army, their Soldier count is colored green in their player area:
<img src="/static/images/rules/largestarmy.png" title="Largest Army"/>

<a id="Notes"></a>

Notes
----

New players often underestimate the power of Development Cards.  It is often the player with 7 points and 3 cards in hand who wins the game instead of the player with 8 points on the board and no cards in hand.  The obvious leader is usually the target of the robber.  Development Cards let a player disguise how many actual points they have.

A well-played Monopoly card should almost always give the player a chance to win the game.