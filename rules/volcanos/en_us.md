Volcanos - Variant
====

<ul>
	<li><a href="info/rules/Volcanos#Overview">Overview</a>
	<li><a href="info/rules/Volcanos#GameBoard">Game Board</a>
	<li><a href="info/rules/Volcanos#InitialPlacements">Initial Placements</a>
	<li><a href="info/rules/Volcanos#Eruptions">Eruptions</a>
	<li><a href="info/rules/Volcanos#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Volcanos variant adds a Volcano tile.

It may be used in the [Standard](/info/scenario/Standard), [New Shores](/info/scenario/NewShores), [Oceans (4)](/info/scenario/Oceans), [Into The Desert](/info/scenario/IntoTheDesert), and [Greater Tacan](/info/scenario/GreaterTacan) scenarios.

It implicitly includes [Gold](/info/rules/Gold) rules.

<a id="GameBoard"></a>

Game Board
----

At the start of the game, all Desert tiles are replaced by Volcanos:

<img src="/static/images/rules/volcanotile.png" title="Vesuvius HUNGERS"/>

A roll chip is randomly generated and placed on each Volcano tile.  It may be any number except 6 or 8.

### Resources

<img src="/static/images/rules/volcano.png" title="Volcano"/>

When the number on a Volcano tile is rolled, each player with a city or settlement on the tile receives gold, __1__ for each settlement, __2__ for each city.  See the [Gold](/info/rules/Gold) rules for details on how gold is spent.

<a id="InitialPlacements"></a>

Initial Placements
----

Volcanos may not be built on during initial placements.

<a id="Eruptions"></a>

Eruptions
----

When a Volcano's number is rolled, it erupts.

If there are settlements or cities on a Volcano when it erupts, after gold has been received and spent, a single 6-sided die is rolled.  The result of this roll is the corner to which the volcano's lava flows.

If any settlement is on that corner, it is destroyed.  The player loses __1__ point and the settlement is returned to their hand.

If any city is on that corner, it is reduced to a settlement.  The player loses __1__ point, the city is returned to their hand, and a settlement is placed on the corner.  If, however, the player has no settlements left in stock (because they are all on the board), the city is, instead, destroyed, and returned to their hand.  The player loses __2__ points.

If multiple volcanos erupt at the same time, the same corner is destroyed on all of them.

<a id="Notes"></a>

Notes
----

Volcanos are dangerous and unpredictable! They add an additional degree of randomness to the game and may result in more cursing than average.

Volcanos constrict the board during initial placements, especially when playing with the full complement of players for a given scenario.  This may result in players starting on corners that are worse than average.

Games played with Volcanos may end more slowly because players lose points and resource production when cities and settlements are reduced or destroyed.

When [Into The Desert](/info/scenario/IntoTheDesert) is played with Volcanos, it is called _Into The Volcanos_.  Playing on _Into The Volcanos_ guarantees an almost uninterrupted stream of eruptions.