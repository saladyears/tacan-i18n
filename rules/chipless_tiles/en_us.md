Chipless Tiles - Rules
====

<ul>
	<li><a href="info/rules/Chipless_Tiles#Overview">Overview</a>
	<li><a href="info/rules/Chipless_Tiles#GameBoard">Game Board</a>
	<li><a href="info/rules/Chipless_Tiles#ChiplessTiles">Chipless Tiles</a>
	<li><a href="info/rules/Chipless_Tiles#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Chipless Tiles rules allow players to strategically move roll chips as they expand their empire.

They are used in the [Greater Tacan](/info/scenario/GreaterTacan) scenario.

<a id="GameBoard"></a>

Game Board
----

At the start of the game, a number of tiles start with no roll chips on them.  These tiles gain roll chips as the game progresses, first from a pool of chips, and then by moving existing roll chips when the pool is exhausted.

<a id="ChiplessTiles"></a>

Chipless Tiles
----

A player gains the right to place a roll chip on a chipless tile by building a road or ship along one of its sides.

The first 7 chips placed are drawn from a pool of __2__, __3__, __4__, __5__, __9__, __10__, and __11__.

When the pool is exhausted, chips are moved from the mainland, using the following rules:

1. No 6s and 8s may be placed on adjacent tiles.
1. The player may not leave a city or settlement on the mainland with no adjacent numbers.
1. The player must take a roll chip from a tile adjacent to one of their cities or settlements.

If any of these rules result in no chips being available for selection, higher number rules are discarded until there are chips available for selection.

For example, if all of the player's cities or settlements on the mainland have only 1 chip left adjacent to them, there would be no chips available for them to select, according to rules 2 and 3.  In this case, rule 3 is discarded and the player may select a chip adjacent to another player's city or settlement (as long as rules 1 and 2 are maintained).

<a id="Notes"></a>

Notes
----

There is strategy involved in moving roll chips.  Players must strike a balance between expanding but not gutting their mainland resource production.