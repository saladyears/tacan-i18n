Repeating Bonus Points - Rules
====

<ul>
	<li><a href="info/rules/Repeating_Bonus_Points#Overview">Overview</a>
	<li><a href="info/rules/Repeating_Bonus_Points#Pieces">Pieces</a>
	<li><a href="info/rules/Repeating_Bonus_Points#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Repeating Bonus Points rules heavily reward players for colonizing new islands.

They are used in the [Islands](/info/scenario/Islands) scenario.

<a id="Pieces"></a>

Pieces
----

### Bonus Points

A player colonizes an island by building a settlement on it.  An island can only be colonized once.  Islands with the player's initial settlements on them cannot be colonized.

The first time a player colonizes an island, the player receives __1__ bonus point:

<img src="/static/images/rules/bonuspoints1.png" title="1 Bonus Point"/>

For each additional island colonized, the player receives __2__ bonus points:

<img src="/static/images/rules/bonuspoints2.png" title="2 Bonus Points"/>

Player's bonus points are tracked in their player area:
<img src="/static/images/rules/bonuspoints.png" title="1 Bonus Point"/>

<a id="Notes"></a>

Notes
----

Getting to the third (or fourth) island and putting down a 3-point settlement can instantly win the game!
