No Early 7s - Variant
====

<ul>
	<li><a href="info/rules/No_Early_7s#Overview">Overview</a>
	<li><a href="info/rules/No_Early_7s#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The No Early 7s variant prevents the number 7 from being rolled during the first round of the game.

It can be used in any scenario.

<a id="Notes"></a>

Notes
----

No Early 7s is the most common variant.  It gives players breathing room at the start of the game and avoids the compounding negative effect of early resource loss to the robber.