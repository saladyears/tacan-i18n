Skip Port Placement - Variant
====

<ul>
	<li><a href="info/rules/Skip_Port_Placement#Overview">Overview</a>
	<li><a href="info/rules/Skip_Port_Placement#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Skip Port Placement variant causes ports to be randomly generated by the computer instead of manually placed by players.

It may be used in the [A New World](/info/scenario/NewWorld) scenario.

<a id="Notes"></a>

Notes
----

There is a greater chance for some truly awful starting positions when ports are computer generated.  It does, however, save a couple minutes at the start of a game!
