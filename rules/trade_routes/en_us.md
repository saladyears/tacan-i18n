Trade Routes - Rules
====

<ul>
	<li><a href="info/rules/Trade_Routes#Overview">Overview</a>
	<li><a href="info/rules/Trade_Routes#InitialPlacements">Initial Placements</a>
	<li><a href="info/rules/Trade_Routes#TradeRoutes">Trade Routes</a>
	<li><a href="info/rules/Trade_Routes#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Trade Routes rules reward players for establishing connections between islands.

They are used in [The Great Crossing](/info/scenario/GreatCrossing) scenario.

<a id="InitialPlacements"></a>

Initial Placements
----

During initial placements, each player must choose one island as their home island.  Both initial placements must be on the same home island.

<a id="TradeRoutes"></a>

Trade Routes
----

Players earn bonus points for completing trade routes.  There is no limit to the number of bonus points that can be earned.

A trade route is a chain of ships and/or roads that connects a city or settlement on one home island to a city or settlement on the other home island.

It does not matter who owns the ships or roads in a trade route, as long as two points on different home islands are connected. Unlike the calculation for Longest Road, a ship that connects to a road with no city or settlement between them _does_ count towards the trade route.

Whenever a trade route is completed, __1__ bonus point is awarded, according to the following rules:

1.  Only the shortest route between the two end points is used.
1.  Only players who own the start or end point of the trade route can receive the bonus point.
1.  If there is one shortest route between the end points, the player with the most pieces in that route receives the point.
1.  If there are multiple shortest routes of the same length between the end points, the player with the most pieces in total over all of the routes receives the point.  (Some pieces will be counted multiple times.)
1.  In the case where the piece counts are equal, the player with the most recently played piece in the route(s) receives the point.

Player's bonus points are tracked in their player area:
<img src="/static/images/rules/bonuspoints.png" title="1 Bonus Point"/>

<a id="Notes"></a>

Notes
----

Because players can move ships, trade route points can change very quickly from turn to turn, particularly because of the "most recently played" rule (#5).

The Trade Routes rules have some of the most subtle interactions in Tacan, especially when the tiles between islands become clogged with many ships that all touch each other.  Players must pay attention!