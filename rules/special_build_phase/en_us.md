Special Build Phase - Rules
====

<ul>
	<li><a href="info/rules/Special_Build_Phase#Overview">Overview</a>
	<li><a href="info/rules/Special_Build_Phase#SpecialBuildPhase">Special Build Phase</a>
	<li><a href="info/rules/Mariners#RuleInteractions">Rule Interactions</a>
	<li><a href="info/rules/Special_Build_Phase#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Special Build Phase rules let players buy items and cards at the end of each player's turn instead of waiting for their turn.  This offers relief for resource cards accumulating in players' hands to the point of losing them to 7 rolls.  

They are automatically included in any game with 5 or 6 players.

Special Build Phase rules are used in the [Standard](/info/scenario/Standard), [Islands](/info/scenario/Islands), [Oceans](/info/scenario/Oceans), [Into The Desert](/info/scenario/IntoTheDesert), [A New World](/info/scenario/NewWorld), [The Great Crossing](/info/scenario/GreatCrossing), and [Greater Tacan](/info/scenario/GreaterTacan) scenarios.

<a id="SpecialBuildPhase"></a>

Special Build Phase
----

After the current player clicks __End Turn__ in the __Actions__ menu, the special build phase begins.

Each other player in the game, starting with the next player and continuing in turn order, has a chance to buy and place items or cards.  When the player is done with their special build phase, they end it by clicking __End Build Phase__ in the __Actions__ menu.  The special build phase then moves on to the next player.

Players may not play cards during the special build phase.

Players may not trade with each other or the bank during the special build phase.  Therefore, in order to make a purchase during their special build phase, a player must have the exact cards in hand for that item.

To avoid giving away any information regarding what cards a player has in their hand, no player is skipped during the special build phase, even if they do not have the cards in hand to purchase anything.

Even during the special build phase, the turn is still the current player's turn.  As players may only win on _their_ turn, even if a player gets to 10 points with a purchase during the special build phase, they do not win the game until it actually becomes their turn.

When every player has ended their special build phase, play moves on to the next player's turn where the dice are rolled, resources are gathered (or not) and they may play a card.

<a id="RuleInteractions"></a>

Rule Interactions
----

<h3><a href="/info/rules/Discovery_Cards">Discovery Cards</a></h3>

Discovery Cards purchased during a special build phase are immediately playable on the player's next turn.

<h3><a href="/info/rules/Mariners">Mariners</a></h3>

Players may buy and place ships during their special build phase.  Ships may not be moved, however.

<h3><a href="/info/rules/Jungles">Jungles</a></h3>

Players may _not_ spend Discovery tokens during the special build phase.  Development cards may only be purchased with Discovery tokens during the player's turn.

<a id="Notes"></a>

Notes
----

Because players are not allowed to trade with each other or the bank during the special build phase, they should trade with the current player as much as possible during the turn to set up their special build phase purchases.