Jungles - Variant
====

<ul>
	<li><a href="info/rules/Jungles#Overview">Overview</a>
	<li><a href="info/rules/Jungles#GameBoard">Game Board</a>
	<li><a href="info/rules/Jungles#DiscoveryTokens">Discovery Tokens</a>
	<li><a href="info/rules/Jungles#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Jungles variant adds a Jungle tile and Discovery tokens.

It may be used in the [Standard](/info/scenario/Standard), [New Shores](/info/scenario/NewShores), [Oceans (4)](/info/scenario/Oceans), [Into The Desert](/info/scenario/IntoTheDesert), and [Greater Tacan](/info/scenario/GreaterTacan) scenarios.

It requires the use of [Development Cards](/info/rules/Development_Cards).

<a id="GameBoard"></a>

Game Board
----

At the start of the game, all Desert tiles are replaced by Jungles:

<img src="/static/images/rules/jungletile.png" title="The Heart of Darkness"/>

A roll chip is randomly generated and placed on each Jungle tile.  It may be any number except 6 or 8.

### Resources

<img src="/static/images/rules/jungle.png" title="Jungle"/>

When a Jungle tile is rolled, each player with a city or settlement on the tile receives Discovery tokens, __1__ for each settlement, __2__ for each city.

There is no limit to the number of Discovery tokens a player can have in hand and the bank never runs out of them.

<p>The number of Discovery tokens a player has played is tracked in their player area:
<img src="/static/images/rules/discoverytokens.png" title="2 Discovery tokens"/>

<a id="DiscoveryTokens"></a>

Discovery Tokens
----

Discovery tokens are used to buy Development Cards.

For each Discovery token a player has, they need one less resource to purchase a Development Card.  In essence, they are wildcards:

<img src="/static/images/rules/discovery1.png" title="1 Discovery token"/>

<img src="/static/images/rules/discovery2.png" title="2 Discovery tokens"/>

If a player has 3 Discovery tokens, a Development Card costs 0 resources:

<img src="/static/images/rules/discovery3.png" title='A "free" Development Card'/>

If it is ambiguous which resources a player should spend with their Discovery tokens, the game will open a Bank Trade window to allow them to choose:

<img src="/static/images/rules/discovery4.png" title="Ambiguous Resource + Discovery tokens"/>

<a id="Notes"></a>

Notes
----

Jungles reduce the costs of Development Cards considerably.  They ensure that Development Cards will be an important part of the game and that more than the average number of them will be purchased.

Games played with Jungles may end more quickly because more Development Cards (and therefore, more Victory Points) are purchased.

When [Into The Desert](/info/scenario/IntoTheDesert) is played with Jungles, it is called _Into The Jungles_. Because so many Discovery tokens are received over the course of the game, _Into The Jungles_ often results in all Development Cards being purchased.