Hidden Tiles - Rules
====

<ul>
	<li><a href="info/rules/Hidden_Tiles#Overview">Overview</a>
	<li><a href="info/rules/Hidden_Tiles#GameBoard">Game Board</a>
	<li><a href="info/rules/Hidden_Tiles#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Hidden Tiles rules reward players for exploring the board and revealing new resource tiles.

They are used in the [Oceans](/info/scenario/Oceans) scenario.

<a id="GameBoard"></a>

Game Board
----

At the start of the game, a number of tiles start off marked as hidden.  An equal number of land and ocean tiles is shuffled and set aside as a pool for when hidden tiles are revealed.

An additional pool of roll chips, equal to the number of resource tiles in the hidden tile pool, is also shuffled and set aside.

### Hidden Tiles

<img src="/static/images/rules/hidden.png" title="Hidden Tile"/>

A player reveals a hidden tile by placing a road or ship along any side that connects to one of the hidden tile's corners.

When a hidden tile is revealed, it is removed.  A tile is then drawn from the tile pool and placed where the hidden tile used to be.

If the new tile is a resource, the player receives __1__ of that resource as a reward.  Additionally, a roll chip is drawn from the roll chip pool and placed on the new resource tile.

<a id="Notes"></a>

Notes
----

Moving ships strategically is key to revealing as many hidden tiles as possible.

If the pool of land and ocean tiles for the scenario contains a Desert (i.e., 4-player [Oceans](/info/scenario/Oceans)), the [Volcanos](/info/rules/Volcanos) and [Jungles](/info/scenario/Jungles) variants can be used.  If one of these variants is used, when the Desert tile is revealed, it is replaced by a Volcano or Jungle, and the player receives __1__ gold (spent immediately) or __1__ Discovery token, respectively.
