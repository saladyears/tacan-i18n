Single Bonus Point - Rules
====

<ul>
	<li><a href="info/rules/Single_Bonus_Point#Overview">Overview</a>
	<li><a href="info/rules/Single_Bonus_Point#Pieces">Pieces</a>
	<li><a href="info/rules/Single_Bonus_Point#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

Single Bonus Point rules reward players for colonizing new islands or lands.

They are used in the [New Shores](/info/scenario/NewShores), [Into The Desert](/info/scenario/IntoTheDesert), and [A New World](/info/scenario/NewWorld) scenarios.

<a id="Pieces"></a>

Pieces
----

### Bonus Point

A player colonizes an island or land by building a settlement on it.  An island or land can only be colonized once.  Islands or lands with the player's initial settlements on them cannot be colonized.

The first time a player colonizes an island or land, the player receives __1__ bonus point:

<img src="/static/images/rules/bonuspoints1.png" title="1 Bonus Point"/>

Players receive no bonus points after the first one, even if they colonize additional islands or lands.

<p>The player's bonus point is tracked in their player area:
<img src="/static/images/rules/bonuspoints.png" title="1 Bonus Point"/>

<a id="Notes"></a>

Notes
----

Do re mi fa so la, ti, and do.