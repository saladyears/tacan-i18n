Mariners - Rules
====

<ul>
	<li><a href="info/rules/Mariners#Overview">Overview</a>
	<li><a href="info/rules/Mariners#Pieces">Pieces</a>
	<li><a href="info/rules/Mariners#InitialPlacements">Initial Placements</a>
	<li><a href="info/rules/Mariners#TurnActions">Turn Actions</a>
	<li><a href="info/rules/Mariners#RuleInteractions">Rule Interactions</a>
	<li><a href="info/rules/Mariners#Notes">Notes</a>
</ul>

<a id="Overview"></a>

Overview
----

The Mariners of Tacan expansion adds seafaring rules to the [Standard](/info/rules/Standard) game.

Mariners is used in the [New Shores](/info/scenario/NewShores), [Islands](/info/scenario/Islands), [Oceans](/info/scenario/Oceans), [Into The Desert](/info/scenario/IntoTheDesert), [A New World](/info/scenario/NewWorld), [The Great Crossing](/info/scenario/GreatCrossing), and [Greater Tacan](/info/scenario/GreaterTacan) scenarios.

<a id="Pieces"></a>

Pieces
----

### Ships

Ships allow players to expand across the ocean to new lands.  They are the maritime equivalent of roads.  

<img src="/static/images/rules/ship.png" title="1 Timber + 1 Sheep = 1 Ship"/>

A ship costs __1 Timber__ and __1 Sheep__.  They are purchased using the __Buy__ menu:

<img src="/static/images/rules/buyship.png" title="Buying a ship"/>

Like roads, ships are placed on the sides of tiles.  They must be placed on a side where two ocean tiles meet, or where an ocean and a land tile meet (a "coast").  They must be placed next to one of the player's cities or settlements, or on a side adjacent to another of the player's ships.  Ships may _not_ be placed adjacent to a player's road unless one of the player's cities or settlements is between them:

<img src="/static/images/rules/placeship.png" title="Placing a ship"/>

Players may build settlements at the end of a chain of ships and, like roads, must leave at least one empty corner between a new settlement and any other settlement or city.

Ships count towards Longest Road, and a Longest Road chain can include both ships and roads.  However, a ship and a road are only considered to connect to each other if one of the player's cities or settlements is between them.

Ships may not be placed on the outer edges of the board or on any side that borders the pirate tile.

Each player starts with __15__ ships.

### The Pirate

The pirate is the robber of the seas.  It starts off the board, lying in wait for unsuspecting prey:

<img src="/static/images/rules/pirate.png" title="Arrrrr!"/>

Like the robber, on a roll of 7, the pirate may be moved from its current tile to any other ocean tile, including ports.  A player may move either the pirate _or_ the robber, but not both.

When the pirate is moved, the player who placed it may steal __1__ resource from any other player with a ship bordering the tile.

The pirate blocks placement of new ships on its tile and prevents existing ships from moving away from its tile.

The pirate does _not_ block usage of a port.

<a id="InitialPlacements"></a>

Initial Placements
----

In Mariners scenarios, players may place roads _or_ ships next to their initial settlements.  

If a player selects a side that borders two land tiles or two ocean tiles, a road or a ship will be placed, respectively.  If one of the tiles is land and the other is ocean, the player can place either a road or a ship, and the game will ask the player which one they want:

<img src="/static/images/rules/selectroadorship.png" title="Choose wisely"/>

<a id="TurnActions"></a>

Turn Actions
----

### Moving a Ship

Once per turn, a player may move one open ship of their choice by clicking __Move Ship__ in the __Actions__ menu.

A ship is considered closed if it is part of a chain connecting two of the player's cities or settlements.  Closed ships cannot be moved:

<img src="/static/images/rules/closedships.png" title="The ships in this chain are closed and cannot be moved"/>

A ship is considered open if it is the "lead" ship in a chain that only connects to one city or settlement.  Open ships can be moved:

<img src="/static/images/rules/openships.png" title="Both ships are open and can be moved"/>

A ship may be moved to any other location on the board where the player could place a new ship.  If there are no valid locations for a ship to move, it cannot be moved.

A ship cannot be moved if it borders the pirate tile.

A ship cannot be moved on the same turn it was bought and placed.

Once a settlement is placed at the end of a chain of ships, that chain of ships is now closed and none of them can be moved.  However, a player can create new branches of ships off the closed chain, allowing the lead ship of the new branch to be moved as desired.

<a id="RuleInteractions"></a>

Rule Interactions
----

<h3><a href="/info/rules/Development_Cards">Development Cards</a></h3>

The Road Building card allows a player to build __2__ roads, __2__ ships, or __1__ road and __1__ ship.

The Soldier card allows a player to move the robber _or_ the pirate.

<a id="Notes"></a>

Notes
----

The addition of the pirate makes 7 rolls more strategic.  If the robber is already on a "good" location, it no longer has to be moved.  It can remain in place while the pirate goes plundering.

The ability to move ships is often forgotten by players new to the Mariners expansion, though it is an important tool that can have a powerful effect on the game.  For example, a player can move an entire chain of ships to a completely different area of the board by moving the "lead" ship every turn, blocking off an opponent's access to new resources.