# Lobby messages.
CreatedGameLobby = You created a new game lobby.
NowOwner = You are now the owner of the lobby.
JoinedGameLobby = You joined a game lobby.
JoinedPublicLobby = You joined the public lobby.
JoinedTheLobby = {0} joined the lobby.
LeftTheLobby = {0} left the lobby.
LobbyDidNotExist = That lobby did not exist.
ChangedScenario = {0} changed the Scenario to {1}.
ChangedNumPlayers = {0} changed the Scenario player count to {1}.
ChangedOption = {0} changed a game option.
YouChangedScenario = You changed the Scenario to {0}.
YouChangedNumPlayers = You changed the Scenario player count to {0}.
YouChangedOption = You changed a game option.

# Game messages.
DroppedFromTheGame = {0} dropped from the game.
LaunchGame = Launched {0} for {1} players.
LeftTheGame = {0} left the game.
GameAbandoned = {0} did not return in time.  The game is abandoned.
GameDidNotExist = That game did not exist.
GameFailed = Critical game failure!  The server can no longer continue this game because: {0}
GameRestarted = The game restarted.
RejoinedTheGame = {0} rejoined the game.
StartedWatching = {0} started watching the game.
StoppedWatching = {0} stopped watching the game.
TriedToLeaveEarly = {0} tried to leave the game early!
YouStartedWatching = You started watching the game.

# Network messages.
ConnectionTimedOut = The connection to the server timed out.
LostConnection = Lost connection to the server.